import React, { Component } from "react";
import { DateRange } from "@appbaseio/reactivesearch";

class Leftbar extends Component {
  render() {
    return (
      <div className={this.props.isClicked ? "left-bar-optional" : "left-bar"}>
        <div className="filter-heading center">
          <b>
            {" "}
            <i className="fa fa-calendar" /> Release Date{" "}
          </b>
        </div>

        <DateRange
          componentId="date-filter"
          dataField="release_date"
          className="datePicker"
        />
        <div className="filter-heading center">
          <b>
            {" "}
            <i className="fa fa-pied-piper-alt" /> Genres{" "}
          </b>
        </div>

        <hr className="blue" />
        <div className="filter-heading center">
          <b>
            {" "}
            <i className="fa fa-language" /> Languages{" "}
          </b>
        </div>

        <MultiDataList
          componentId="language-list"
          dataField="original_language.raw"
          className="language-filter"
          size={100}
          sortBy="asc"
          queryFormat="or"
          selectAllLabel="All Languages"
          showCheckbox={true}
          showSearch={true}
          placeholder="Search for a language"
          react={{
            and: [
              "mainSearch",
              "results",
              "date-filter",
              "RangeSlider",
              "genres-list",
              "revenue-list"
            ]
          }}
          data={[
            {
              label: "English",
              value: "English"
            },
            {
              label: "Chinese",
              value: "Chinese"
            },
            {
              label: "Turkish",
              value: "Turkish"
            },
            {
              label: "Swedish",
              value: "Swedish"
            },
            {
              label: "Russian",
              value: "Russian"
            },
            {
              label: "Portuguese",
              value: "Portuguese"
            },
            {
              label: "Korean",
              value: "Korean"
            },
            {
              label: "Japanese",
              value: "Japanese"
            },
            {
              label: "Italian",
              value: "Italian"
            },
            {
              label: "Hindi",
              value: "Hindi"
            },
            {
              label: "French",
              value: "French"
            },
            {
              label: "Finnish",
              value: "Finnish"
            },
            {
              label: "Spanish",
              value: "Spanish"
            },
            {
              label: "Deutsch",
              value: "Deutsch"
            }
          ]}
          showFilter={true}
          filterLabel="Language"
          URLParams={false}
          innerClass={{
            label: "list-item",
            input: "list-input"
          }}
        />

        <hr className="blue" />
      </div>
    );
  }
}
export default Leftbar;
